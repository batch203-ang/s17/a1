//console.log("Hello, World!");


//-----------------Number 1--------------------
function getUserInfo(){
	let fullName = prompt("Enter Full Name:");
	let age = prompt("Enter Age:");
	let location = prompt("Enter Address:");
	console.log("Hello, " + fullName + "!");
	console.log("You are " + age + " years old.");
	console.log("You live in " + location + ".");
}
getUserInfo();

//-----------------Number 2--------------------
function displayArtist(){
	console.log("1. The Beatles");
	console.log("2. Metallica");
	console.log("3. The Eagles");
	console.log("4. L'arc~en~Ciel");
	console.log("5. Eraserheads");
}
displayArtist();

//-----------------Number 3--------------------
function displayMovies(){
	console.log("1. The Godfather");
	console.log("Rotten Tomatoes, Rating: 97%");
	console.log("2. The Godfather II");
	console.log("Rotten Tomatoes, Rating: 96%");
	console.log("3. Shawshank Redemption");
	console.log("Rotten Tomatoes, Rating: 91%");
	console.log("4. To Kill a Mocking Bird");
	console.log("Rotten Tomatoes, Rating: 93%");
	console.log("5. Psycho");
	console.log("Rotten Tomatoes, Rating: 96%");
}
displayMovies();

//-----------------Number 4--------------------
let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();
//console.log(friend1);
//console.log(friend2);